package com.fox.pbcs.box;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.box.sdk.BoxAPIConnection;
import com.box.sdk.BoxAPIResponseException;
import com.box.sdk.BoxFile;
import com.box.sdk.BoxFolder;
import com.box.sdk.BoxItem;
import com.box.sdk.BoxUser;

public final class Main {

	private static final String CLIENT_ID = "CLIENT_ID";
	private static final String CLIENT_SECRET = "CLIENT_SECRET";
	private static final String ACCESS_TOKEN = "ACCESS_TOKEN";
	private static final String REFRESH_TOKEN = "REFRESH_TOKEN";
	private static final String ULTIMATE_FOLDER = "ULTIMATE_FOLDER";
	private static final String FIS_INBOUND_FOLDER = "FIS_INBOUND_FOLDER";
	private static final String ULTIMATE_CSV_FILE = "ULTIMATE_CSV_FILE";
	private static final String ULTIMATE_ZIP_FILE = "ULTIMATE_ZIP_FILE";

	private static Properties props;

	private Main() {
	}

	public static void main(String[] args) {
		// Turn off logging to prevent polluting the output.
		Logger.getLogger("com.box.sdk").setLevel(Level.FINEST);

		String boxPropertyFile = System.getProperty("box.configuration");

		if (boxPropertyFile == null) {
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			System.out.println(dateFormat.format(date) + "Command line argurment -Dbox.configuration missing");
			System.exit(-1);
		}

		try {
			process(boxPropertyFile);
			System.exit(0);
		} catch (IOException e) {
			e.printStackTrace();
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			System.out.println(dateFormat.format(date) + "Caught exception "+ e.getMessage());
			System.exit(-1);
		}

	}

	private static void process(String boxPropertyFile) throws IOException {

		props = PropertyUtil.readProperties(boxPropertyFile);

		String clientId = props.getProperty(CLIENT_ID);
		String clientSecret = props.getProperty(CLIENT_SECRET);
		String refreshToken = props.getProperty(REFRESH_TOKEN);
		String accessToken = props.getProperty(ACCESS_TOKEN);

		BoxAPIConnection api = new BoxAPIConnection(clientId, clientSecret, accessToken, refreshToken);

		// setProxy(api);

		api.setAutoRefresh(false);

		try {
			// check whether access token is still valid
			checkToken(api);

			downloadUltimateFile(api);

		} catch (BoxAPIResponseException ex) {
			// System.out.println("Refreshing the token");
			api.refresh();
			props.setProperty(REFRESH_TOKEN, api.getRefreshToken());
			props.setProperty(ACCESS_TOKEN, api.getAccessToken());

			// Update new token in box.properties
			PropertyUtil.writeProperties(boxPropertyFile, props);
			// FileUtil.writeTokenToFile("refreshToken.txt", api.getRefreshToken());
			// FileUtil.writeTokenToFile("accessToken.txt", api.getAccessToken());

			downloadUltimateFile(api);
		}

	}

	/*
	 * private static void setProxy(BoxAPIConnection api) {
	 * 
	 * String proxyHost = props.getProperty(PROXY_HOST); int proxyPort = 8080;
	 * 
	 * 
	 * 
	 * if(proxyHost != null) {
	 * 
	 * if(props.getProperty(PROXY_PORT) != null) { proxyPort =
	 * Integer.parseInt(props.getProperty(PROXY_PORT)); }
	 * 
	 * Proxy proxy = null; try { proxy = new Proxy(Type.HTTP, new
	 * InetSocketAddress(InetAddress.getByName(proxyHost), proxyPort)); } catch
	 * (UnknownHostException e) { e.printStackTrace(); } api.setProxy(proxy); }
	 * 
	 * 
	 * }
	 */

	private static void checkToken(BoxAPIConnection api) throws FileNotFoundException, BoxAPIResponseException {
		try {

			BoxUser.Info userInfo = BoxUser.getCurrentUser(api).getInfo();
			// System.out.format("Welcome, %s <%s>!\n\n", userInfo.getName(),
			// userInfo.getLogin());
			// System.out.println(api.getExpires());
		} catch (BoxAPIResponseException ex) {
			// System.out.println("Caught exception in checkToken");
			throw ex;
		}

	}

	private static void downloadUltimateFile(BoxAPIConnection api) throws FileNotFoundException, IOException {

		BoxFolder ultimatFolder = new BoxFolder(api, props.getProperty(ULTIMATE_FOLDER));
		String fileId = getUltimateFileId(ultimatFolder);
		
		String ultimateFolder = props.getProperty(FIS_INBOUND_FOLDER);
		String csvFileName = props.getProperty(ULTIMATE_CSV_FILE);
		String zipFileName = props.getProperty(ULTIMATE_ZIP_FILE);

		if (fileId != null) {
			// System.out.println(fileId);

			File csvFile = new File(ultimateFolder + File.separator + csvFileName);			
			File zipFile = new File(ultimateFolder + File.separator + zipFileName);
			
			//Locate Ultimate file in Box
			BoxFile ultimateBoxFile = new BoxFile(api, fileId);

			//download file from box
			downloadFile(ultimateBoxFile, csvFile);
			
			//zip file in FIS Inbox
			ZipUtil.zip(csvFile, zipFile);
			
			// delete File from box
			ultimateBoxFile.delete();

			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			System.out.println(dateFormat.format(date)
					+ ": Ultimate File downloaded from box; Placed both CSV and Zip file in Inbound folder");

		}else {
			
		}

	}

	/**
	 * Download File
	 * 
	 * @param file
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private static void downloadFile(BoxFile boxFile, File csvFile) throws FileNotFoundException, IOException {

		// BoxFile.Info info = file.getInfo();
		
		
		if(csvFile.exists()) {
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			System.out.println(dateFormat.format(date)+": Exiting, Unprocessed Ultimate Zip file still exist in FIS Inbox , Will try to download file from box in next job schedule run");
			System.exit(-1);
		}else {
			FileOutputStream stream = new FileOutputStream(csvFile);
			boxFile.download(stream);
			stream.close();
		}
		
	}

	/**
	 * Get Ultimate File Id
	 * 
	 * @param folder
	 * @return
	 */
	private static String getUltimateFileId(BoxFolder folder) {
		String fileId = null;
		for (BoxItem.Info itemInfo : folder) {

			if (props.getProperty(ULTIMATE_CSV_FILE).equals(itemInfo.getName())) {
				fileId = itemInfo.getID();
				break;
			}
		}
		return fileId;
	}
}
