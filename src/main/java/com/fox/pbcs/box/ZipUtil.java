package com.fox.pbcs.box;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipUtil {
	
	public static void zip(File file, File zipFile) throws IOException
    {
    	byte[] buffer = new byte[1024];
    	
    	try{
    		
    		FileOutputStream fos = new FileOutputStream(zipFile);
    		ZipOutputStream zos = new ZipOutputStream(fos);
    		ZipEntry ze= new ZipEntry(file.getName());
    		zos.putNextEntry(ze);
    		
    		FileInputStream in = new FileInputStream(file);
   	   
    		int len;
    		while ((len = in.read(buffer)) > 0) {
    			zos.write(buffer, 0, len);
    		}

    		in.close();
    		zos.closeEntry();
           
    		//remember close it
    		zos.close();          
    		//System.out.println("Done");

    	}catch(IOException ex){
    	   ex.printStackTrace();
    	   throw ex;
    	}
    }

}
