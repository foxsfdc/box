package com.fox.pbcs.box;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PropertyUtil {
	
	
	public static Properties readProperties(String fileName) throws IOException {
		
		Properties props = new Properties();
		InputStream input = null;
		Map<String, String> propertyMap = new HashMap<String, String>();

		try {

			input = new FileInputStream(fileName);

			// load a properties file
			props.load(input);
			
			

		} catch (IOException ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return props;
	}
	
	
	public static void writeProperties(String fileName, Properties props) throws IOException {
		
		//Properties prop = new Properties();
		OutputStream output = null;

		try {

			output = new FileOutputStream(fileName);
			
			// save properties to project root folder
			props.store(output, null);

		} catch (IOException io) {
			io.printStackTrace();
			throw io;
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
		
	}

}
